#!/usr/bin/env bash

input_file="./cit-HepPh.txt"
line_num=`wc -l < $input_file`
python_script="./k_clique_percolation.py"
log_file="./cit-HepPh_strat1/running.log"
echo "Percolation algorithm start" > $log_file
declare -a sample_rates
sample_rates=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9)
for sample_rate in ${sample_rates[@]}
do
echo "Start doing sample rate "$sample_rate >> $log_file
sample_size=`echo $line_num \* $sample_rate | bc | awk '{print int($0)}'`
output_path="./cit-HepPh_strat1/result_"$sample_rate".txt"
python $python_script --file_path $input_file --sample_size $sample_size --output_path $output_path --strategy "new" >> $log_file
done
