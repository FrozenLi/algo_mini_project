import networkx as nx
import random
import argparse
from collections import deque
import math
import time
from numpy.matlib import repmat
'''
test=nx.Graph()
test.add_nodes_from([1,2,3,4,5,6,7])
test.add_edge(1,2)
test.add_edge(2,3)
test.add_edge(1,3)
test.add_edge(1,4)
test.add_edge(2,4)
test.add_edge(5,6)
test.add_edge(5,4)
test.add_edge(4,6)
c = list(nx.algorithms.community.k_clique_communities(test,3))
print(type(c[0]))
print(c[1])
for i in c[0]:
    print (i)
'''
def process_string(input_string):
    #temp fix for new strat
    if isinstance(input_string,tuple):
        return input_string[0],input_string[1]
    temp=input_string.split()
    node1=int(temp[0].strip())
    node2=int(temp[1].strip())
    return node1,node2

def read_graph(args,G):
    filename=args.file_path
    temp=[]
    with open(filename,'r') as f:
        for row in f:
            temp.append(row)
    if args.shuffle==True:
        random.shuffle(temp)

    sampled_edges=temp[0:args.sample_size]
    unsampled_edges=temp[args.sample_size:]
    nodes=set()
    edges=[]
    for i in sampled_edges:
        node1,node2=process_string(i)
        nodes.add(node1)
        nodes.add(node2)
        edges.append((node1,node2))
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    return unsampled_edges,G

def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--shuffle", default=True, type=bool, help="Whether shuffle or not")
    parser.add_argument("--file_path",default="./cit-HepPh.txt",type=str,help="input data path")
    parser.add_argument("--sample_size",default=100000,type=int,help="sample size")
    parser.add_argument("--output_path",default="./output.txt",type=str,help="output file path")
    parser.add_argument("--strategy",default="old",type=str,help="which strategy to use -- [old][new]")
    args = parser.parse_args()
    return args

def read_graph_strat1(args,G):
    filename=args.file_path
    temp=[]
    with open(filename,'r') as f:
        for row in f:
            temp.append(row)
    nodes=set()
    edges=[]
    for i in temp:
        node1,node2=process_string(i)
        nodes.add(node1)
        nodes.add(node2)
        edges.append((node1,node2))
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    #sort nodes according to degree
    node_deg=list(G.degree)
    node_deg.sort(key=lambda tup:tup[1],reverse=True)
    sampled_size=0
    sampled_graph=nx.Graph()
    unsampled_graph=G.copy()
    for i in node_deg:
        if sampled_size < args.sample_size:
            sampled_graph.add_node(i[0])
            for neighbor in G.neighbors(i[0]):
                if sampled_graph.has_edge(i[0],neighbor):
                    continue
                sampled_graph.add_node(neighbor)
                sampled_graph.add_edge(i[0],neighbor)
                sampled_size+=1
                unsampled_graph.remove_edge(i[0],neighbor)

        else:
            break
    return list(unsampled_graph.edges),sampled_graph




def BFS_search(graph,node):
    visited_nodes=set()
    if 'cluster' in graph.node[node]:
        return graph
    else:
        visited_nodes.add(node)
        to_visit=deque()
        for neighbor in graph.neighbors(node):
            to_visit.append(neighbor)
        while to_visit:
            check_node=to_visit.popleft()
            if check_node not in visited_nodes:
                if 'cluster' in graph.node[check_node]:
                    break
                else:
                    visited_nodes.add(check_node)
                    for neighbor in graph.neighbors(check_node):
                        to_visit.append(neighbor)
        if 'cluster' in graph.node[check_node]:
            for visited_node in visited_nodes:
                graph.node[visited_node]['cluster']=[graph.node[check_node]['cluster'][0]]
        return graph

def modularity(graph):
    #sum(e_ii-a_i^2)
    total_edge=nx.number_of_edges(graph)
    temp_dict={}
    for node in nx.nodes(graph):
        for neighbor_node in graph.neighbors(node):
            key = graph.node[node]['cluster'][0]
            if key in graph.node[neighbor_node]['cluster']:
                if key not in temp_dict.keys():
                    temp_dict[key]=(1,0)
                else:
                    temp_dict[key]=(temp_dict[key][0]+1,temp_dict[key][1])
            else:
                if key not in temp_dict.keys():
                    temp_dict[key]=(0,1)
                else:
                    temp_dict[key]=(temp_dict[key][0],temp_dict[key][1]+1)
    modularity=0
    for key in temp_dict.keys():
        modularity+=(temp_dict[key][0]/(total_edge*2)-math.pow(temp_dict[key][1]/(total_edge*2),2))
    return modularity










if __name__ =="__main__":
    args=arg_parser()
    start=time.clock()
    G=nx.Graph()
    #randam sample
    if args.strategy=="old":
        unsampled_edges,G=read_graph(args,G)
    elif args.strategy=="new":
        unsampled_edges,G=read_graph_strat1(args,G)
    #Clustering and labelling for sampled nodes
    clusters=list(nx.algorithms.community.k_clique_communities(G,3))
    for cluster_label in range(0,len(clusters)):
        for single_node in clusters[cluster_label]:
            if 'cluster' not in G.node[single_node]:
                G.node[single_node]['cluster']=[cluster_label]
            else:
                G.node[single_node]['cluster'].append(cluster_label)
    check_point1_time=(time.clock()-start)
    check_point1=time.clock()
    print("complete 1st part - clustering")
    print("time used {}".format(check_point1_time))
    #read the rest nodes and clustering them

    for row in unsampled_edges:
        node1,node2=process_string(row)
        G.add_nodes_from([node1,node2])
        G.add_edge(node1,node2)
        #BFS from node 1, once meet one node already have cluter then stop and label all visited nodes as this cluster
        G=BFS_search(G,node1)
        G=BFS_search(G,node2)
    check_point2_time=(time.clock()-check_point1)
    check_point2=time.clock()
    print("complete 2nd part - update all nodes' cluster")
    print("time used {}".format(check_point2_time))
    print("start writing result")
    writer=open(args.output_path,'w')
    for node in G.nodes(data=False):
        if 'cluster' in G.node[node]:
            to_write=str(node)+","+",".join([str(i) for i in G.node[node]['cluster']])+'\n'
        else:
            G.node[node]['cluster']=[len(clusters)]
            to_write=str(node)+","+",".join([str(i) for i in G.node[node]['cluster']])+'\n'
        writer.write(to_write)
    writer.close()
    print("Percolation program complete")
    print("Modularity is {}".format(modularity(G)))


