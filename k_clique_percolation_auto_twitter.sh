#!/usr/bin/env bash

input_file="./twitter_combined.txt"
line_num=`wc -l < $input_file`
python_script="./k_clique_percolation.py"
log_file="./twitter_combined/running.log"
echo "Percolation algorithm start" > $log_file
declare -a sample_rates
sample_rates=(0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0)
for sample_rate in ${sample_rates[@]}
do
echo "Start doing sample rate "$sample_rate >> $log_file
sample_size=`echo $line_num \* $sample_rate | bc | awk '{print int($0)}'`
output_path="./twitter_combined/result_"$sample_rate".txt"
python $python_script --file_path $input_file --sample_size $sample_size --output_path $output_path >> $log_file
done
